/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/23 14:28:22 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/23 14:30:12 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
/*
**	void	*ft_realloc(void *p, size_t size)
**	{
**		void *temp;
**
**		if (!p)
**			return (malloc(size));
**		else if (!size)
**		{
**			free(p);
**			p = NULL;
**		}
**		else if (malloc_size(p) == malloc_good_size(size))
**			return (p);
**		else
**		{
**			temp = malloc(malloc_size(p));
**			ft_memcpy(temp, p, malloc_size(p));
**			free(p);
**			p = malloc(size);
**			if (!p)
**				perror("Memory was not allocated:\n");
**			ft_memcpy(p, temp, malloc_size(temp));
**			free(temp);
**		}
**		return (p);
**	}
*/
