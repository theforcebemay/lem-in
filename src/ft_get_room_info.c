/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fetch_info.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/12 17:04:56 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/23 14:13:43 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

int					get_room_info(const char *line_content, unsigned int status)
{
	t_room			*r;
	char			**hm;
	static unsigned	id = 0;

	r = ft_calloc(1, sizeof(t_room));
	g_all_rooms[id] = r;
	if (id + 1 == MAX_ROOM)
		ft_error("Mnogo komnat\nWrite own realloc and have fun!\n");
	r->id = id++;
	r->status = status;
	hm = ft_strsplit(line_content, ' ');
	ft_strcpy(r->name, hm[0]);
	if ((hm[1][0] == '0' && hm[1][1] != '\0')
			|| (hm[2][0] == '0' && hm[2][1] != '\n'))
		ft_error_msg();
	r->x = ft_atoi(hm[1]);
	r->y = ft_atoi(hm[2]);
	is_valid_x_y(r->x, r->y, id - 1);
	is_valid_name(r->name, id - 1);
	free(hm[0]);
	free(hm[1]);
	free(hm[2]);
	free(hm);
	return (EXIT_SUCCESS);
}

unsigned			ft_paths_lenths(t_llist *paths)
{
	unsigned		total;
	t_llist			*temp_link;

	total = 0;
	temp_link = paths;
	while (temp_link)
	{
		total = ((t_route *)(temp_link->data))->len;
		temp_link = temp_link->next;
	}
	return (total);
}

t_llist				*ft_accumulate_paths(t_llist *routes)
{
	t_llist			*selected_routes;
	t_route			*best_route;
	unsigned		total_length;

	selected_routes = NULL;
	total_length = 0;
	while (total_length < (unsigned)g_ants)
	{
		best_route = ft_take_the_shortest_path(routes);
		if (!best_route)
			break ;
		ft_ldcpushb(&selected_routes, best_route, sizeof(t_route));
		total_length += ft_paths_lenths(selected_routes);
	}
	return (selected_routes);
}

void				allocate_adj(void)
{
	g_adj->adj = alloadjmatrix(room_count(g_all_rooms));
	g_adj->size = room_count(g_all_rooms);
}
