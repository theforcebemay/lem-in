/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   useful.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/30 20:21:39 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/23 14:10:39 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

int		ft_replace(char *where, char what, char to)
{
	where = ft_strchr(where, what);
	if (!where)
		return (EXIT_FAILURE);
	where[0] = to;
	return (EXIT_SUCCESS);
}

int		exit_if_empty(const char *line_content)
{
	if (!line_content || line_content[0] == '\n' || line_content[0] == '\0')
		ft_error_msg();
	return (EXIT_SUCCESS);
}

void	ft_error_msg(void)
{
	write(1, "Error\n", 6);
	exit(18);
}

int		is_adj(unsigned v, unsigned i)
{
	return (g_adj->adj[v][i]);
}
