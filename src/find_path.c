/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_path.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/12 17:04:48 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/23 14:20:14 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

t_route			*ft_btrack_one_route(unsigned v)
{
	unsigned	i;
	t_route		*route;

	i = 0;
	ft_adjust_route_structure(&route, v);
	while (i < (unsigned)g_adj->size)
	{
		if (is_start_room(v, route))
			break ;
		else if (is_adj(v, i) && goes_to_start(v, i)
							&& !g_all_rooms[i]->tracked)
		{
			ft_ldcpushf(&route->route, &i, sizeof(unsigned));
			if (g_all_rooms[i]->status != START_ROOM)
				g_all_rooms[i]->tracked = 1;
			v = i;
			i = 0;
			continue;
		}
		i++;
	}
	if (i == (unsigned)g_adj->size)
		ft_delete_route(&route);
	return (route);
}

t_llist			*ft_obtain_all_routes(unsigned v)
{
	t_llist		*all_routes;
	t_llist		*queue;
	t_route		*backtracked_route;
	unsigned	*adjverticetoendroom;

	all_routes = NULL;
	queue = ft_adj(v);
	while (queue)
	{
		adjverticetoendroom = (unsigned *)ft_ldpop(&queue);
		backtracked_route = ft_btrack_one_route(*adjverticetoendroom);
		if (backtracked_route)
		{
			backtracked_route->len = ft_llen(backtracked_route->route);
			ft_ldcpushb(&all_routes, backtracked_route, sizeof(t_route));
			free(backtracked_route);
		}
		free(adjverticetoendroom);
	}
	return (all_routes);
}

t_llist			*pre_solve(t_llist *routes)
{
	unsigned	start;
	unsigned	end;
	int			*visited;
	int			k;

	k = room_count(g_all_rooms);
	visited = ft_calloc((size_t)k, sizeof(int));
	start = (unsigned)search_by_status(START_ROOM, g_all_rooms);
	end = (unsigned)search_by_status(END_ROOM, g_all_rooms);
	bfs(start, end, visited);
	routes = ft_obtain_all_routes(end);
	return (routes);
}

void			ft_march_room(unsigned now, unsigned next,
									unsigned e, unsigned *ants)
{
	if (g_all_rooms[now]->status == END_ROOM)
	{
		if (g_all_rooms[next]->antn)
		{
			ft_answer(next, e);
			g_all_rooms[next]->antn = 0;
			(*ants)--;
		}
		else if (g_all_rooms[next]->status == START_ROOM)
			ft_blitzkrieg(now, ants);
	}
	else if (g_all_rooms[next]->status == START_ROOM && *ants)
	{
		g_all_rooms[now]->antn = ft_take_ant();
		ft_answer(now, now);
	}
	else if (g_all_rooms[next]->antn && g_all_rooms[next]->status != START_ROOM)
	{
		g_all_rooms[now]->antn = g_all_rooms[next]->antn;
		g_all_rooms[next]->antn = 0;
		ft_answer(now, now);
	}
}

void			ft_march_list_cycle(t_route *path_struct, unsigned *ants)
{
	t_llist		*templist;
	unsigned	now;
	unsigned	next;

	templist = path_struct->route;
	while (templist->next)
	{
		now = *(unsigned *)templist->data;
		next = *(unsigned *)templist->next->data;
		ft_march_room(now, next, path_struct->e, ants);
		templist = templist->next;
	}
}
