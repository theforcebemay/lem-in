/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_parse.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/29 15:34:18 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/23 14:10:09 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

int			get_link(const char *line_content, int **adj)
{
	int		id1;
	int		id2;
	char	**hm;

	g_parse_status = 3;
	get_validate_rooms(g_all_rooms);
	hm = ft_strsplit(line_content, '-');
	ft_replace(hm[1], '\n', '\0');
	id1 = search_name(g_all_rooms, hm[0]);
	id2 = search_name(g_all_rooms, hm[1]);
	if (id1 < 0 || id2 < 0)
		ft_error_msg();
	id1 = g_all_rooms[id1]->id;
	id2 = g_all_rooms[id2]->id;
	if (!adj[id1][id2])
	{
		adj[id1][id2] = TRUE;
		adj[id2][id1] = TRUE;
	}
	free(hm[0]);
	free(hm[1]);
	free(hm);
	return (EXIT_SUCCESS);
}

char		*ft_jump_comments(int fd)
{
	char	*line;

	while (TRUE)
	{
		line = get_next_line_fd(fd);
		exit_if_empty(line);
		if (is_room(line))
			return (line);
		else if (is_comment(line))
			free(line);
		else
			ft_error_msg();
	}
}

int			room_record(const char *line_content, int fd)
{
	char	*line;

	if (g_parse_status == 1 || g_parse_status == 2)
		g_parse_status = 2;
	else
		ft_error_msg();
	if (is_command_start(line_content))
	{
		line = ft_jump_comments(fd);
		get_room_info(line, START_ROOM);
		free(line);
	}
	else if (is_command_end(line_content))
	{
		line = ft_jump_comments(fd);
		get_room_info(line, END_ROOM);
		free(line);
	}
	else if (is_room(line_content))
		get_room_info(line_content, REGULAR_ROOM);
	return (0);
}

void		ft_do_nothing(void)
{
}

int			parse_line(const char *line_content, int fd)
{
	if (is_command_links(line_content))
		ft_do_nothing();
	else if (is_comment(line_content))
		;
	else if (is_command_start(line_content) ||
			is_command_end(line_content) ||
			is_room(line_content))
		room_record(line_content, fd);
	else if (ft_strchr(line_content, '-'))
		get_link(line_content, g_adj->adj);
	else
		ft_error_msg();
	return (EXIT_SUCCESS);
}
