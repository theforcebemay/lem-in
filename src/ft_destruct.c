/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_destruct.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/23 13:56:24 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/23 13:56:41 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

/*
**	Recursive freeing of list
*/

void	ft_delete_route(t_route **r)
{
	ft_ldela((*r)->route);
	free(*r);
	*r = NULL;
}

void	ft_ldelastruct(t_llist *head)
{
	t_route	*t;

	if (head->next)
		ft_ldelastruct(head->next);
	t = head->data;
	ft_ldela(t->route);
	free(head->data);
	free(head);
}

void	ft_ldela(t_llist *head)
{
	if (head)
	{
		if (head->next)
			ft_ldela(head->next);
		free(head->data);
		free(head);
	}
}
