/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   detect.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/29 18:29:30 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/12 18:30:46 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

void	is_valid_name(char *name_searh, unsigned stop)
{
	unsigned i;

	i = 0;
	while (g_all_rooms[i] && i < stop)
	{
		if (!ft_strcmp(name_searh, g_all_rooms[i]->name))
			ft_error_msg();
		i++;
	}
}

void	is_valid_x_y(unsigned x, unsigned y, unsigned stop)
{
	unsigned i;

	i = 0;
	while (g_all_rooms[i] && i < stop)
	{
		if ((g_all_rooms[i]->x == (int)x) && (g_all_rooms[i]->y == (int)y))
			ft_error_msg();
		i++;
	}
}

int		is_command_links(const char *line_content)
{
	if (!ft_strcmp("##links\n", line_content))
		return (EXIT_FAILURE);
	return (EXIT_SUCCESS);
}

int		is_command_start(const char *line_content)
{
	if (!ft_strcmp("##start\n", line_content))
		return (EXIT_FAILURE);
	return (EXIT_SUCCESS);
}
