/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rooms_duplicate_check.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/12 17:05:03 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/07/12 17:05:36 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

int		get_validate_rooms(t_room **room)
{
	int	start;
	int	end;
	int i;

	start = 0;
	end = 0;
	i = 0;
	while (room[i])
	{
		if (room[i]->status == START_ROOM)
			start++;
		else if (room[i]->status == END_ROOM)
			end++;
		i++;
	}
	if (end == 1 && start == 1)
		return (EXIT_SUCCESS);
	ft_error_msg();
	return (EXIT_FAILURE);
}
