/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_path_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/23 13:51:52 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/23 14:10:29 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

int		goes_to_start(unsigned v, unsigned i)
{
	return ((g_all_rooms[v]->path) == (g_all_rooms[i]->path + 1));
}

int		is_start_room(unsigned v, t_route *route)
{
	return (route->s == v) ? (1) : (0);
}

/*
**	Return list of adjacent edges
*/

t_llist	*ft_adj(unsigned v1)
{
	unsigned	v;
	int			**adg;
	t_llist		*adjacent;

	adjacent = NULL;
	adg = g_adj->adj;
	v = 0;
	while (v < (unsigned)g_adj->size)
	{
		if (adg[v1][v])
			ft_ldcpushb(&adjacent, &v, sizeof(unsigned));
		v++;
	}
	return (adjacent);
}

t_route	*ft_take_the_shortest_path(t_llist *routes)
{
	t_llist	*temp_list;
	t_route	*the_fastest;
	t_route	*temp_path;

	the_fastest = NULL;
	temp_list = routes;
	while (temp_list)
	{
		temp_path = ft_the_best_possible_path(temp_list->data);
		if (temp_path)
			the_fastest = temp_path;
		temp_list = temp_list->next;
	}
	if (the_fastest)
		the_fastest->used = 1;
	ft_the_best_possible_path(NULL);
	return (the_fastest);
}

t_route	*ft_the_best_possible_path(t_route *path)
{
	static int lowest;

	if (!path)
	{
		lowest = 0;
		return (NULL);
	}
	if (path->used)
		return (NULL);
	if (!lowest)
	{
		lowest = path->len;
		return (path);
	}
	if (path->len <= (unsigned)lowest)
	{
		lowest = path->len;
		return (path);
	}
	return (NULL);
}
