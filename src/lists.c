/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lists.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 15:17:33 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/23 14:02:41 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lists.h"
#include "../libft/libft.h"
#include "../include/lemin.h"

unsigned		ft_llen(t_llist *head)
{
	t_llist		*temp;
	unsigned	c;

	c = 0;
	temp = head;
	while (temp)
	{
		temp = temp->next;
		c++;
	}
	return (c);
}

/*
**	ft_ListDataCopyPushFront
*/

void			ft_ldcpushf(t_llist **head_ref, void *new_data, size_t size)
{
	t_llist		*new_node;

	new_node = ft_calloc(1, sizeof(t_llist));
	if (head_ref)
	{
		new_node->data = ft_calloc(1, size);
		new_node->next = (*head_ref);
		memcpy(new_node->data, new_data, size);
		*head_ref = new_node;
	}
}

/*
**  ft_ListDataCopy push Back
*/

void			ft_ldcpushb(t_llist **head_ref, void *new_data, size_t size)
{
	t_llist		*node;
	t_llist		*temp;

	if (head_ref)
	{
		node = (t_llist *)ft_calloc(1, sizeof(t_llist));
		node->data = ft_calloc(1, size);
		ft_memcpy(node->data, new_data, size);
		if (!*head_ref)
			*head_ref = node;
		else
		{
			temp = *head_ref;
			while (temp->next)
				temp = temp->next;
			temp->next = node;
		}
	}
}

/*
**  ft_ListData pop
*/

void			*ft_ldpop(t_llist **head_ref)
{
	t_llist		*node;
	void		*data;

	if (!head_ref || !*head_ref)
		return (NULL);
	node = *head_ref;
	*head_ref = node->next;
	data = node->data;
	free(node);
	return (data);
}
