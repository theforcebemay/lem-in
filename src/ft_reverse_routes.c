/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_asses.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 15:17:28 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/12 18:33:35 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

void		ft_lrev(t_llist **head_ref)
{
	t_llist	*curr;
	t_llist	*next;
	t_llist	*prev;

	curr = *head_ref;
	prev = NULL;
	while (curr)
	{
		next = curr->next;
		curr->next = prev;
		prev = curr;
		curr = next;
	}
	*head_ref = prev;
}

void		ft_reverse_routes(t_llist *routes)
{
	t_route	*tmp_route;
	t_llist	*tmp_llist;

	tmp_llist = routes;
	while (tmp_llist)
	{
		tmp_route = tmp_llist->data;
		ft_lrev(&tmp_route->route);
		tmp_llist = tmp_llist->next;
	}
}
