/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slists.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 15:17:42 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/12 18:43:23 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

int			room_count(t_room **rooms)
{
	int		count;

	count = 0;
	while (rooms[count])
		count++;
	return (count);
}
