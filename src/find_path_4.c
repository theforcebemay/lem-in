/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_path_4.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/23 13:55:07 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/23 13:56:06 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

void		ft_march_cycle(t_llist *routes, unsigned *ants)
{
	t_llist	*temp_llist;

	temp_llist = routes;
	while (temp_llist)
	{
		ft_march_list_cycle((t_route *)temp_llist->data, ants);
		temp_llist = temp_llist->next;
	}
}

void		ft_reverse_marching_tree(t_llist *routes)
{
	int		*ants;

	ants = ft_calloc(1, sizeof(unsigned));
	*ants = (unsigned)g_ants;
	while ((*ants) > 0)
	{
		ft_march_cycle(routes, (unsigned *)ants);
		write(1, "\n", 1);
	}
	free(ants);
}

void		ft_answer(unsigned ant_room_id, unsigned room_name)
{
	int		ant;

	ant = g_all_rooms[ant_room_id]->antn;
	if (ant)
	{
		write(1, "L", 1);
		ft_putnbr(g_all_rooms[ant_room_id]->antn);
		write(1, "-", 1);
		ft_putstr(g_all_rooms[room_name]->name);
		write(1, " ", 1);
	}
}

void		ft_blitzkrieg(unsigned now, unsigned *ants)
{
	g_all_rooms[now]->antn = ft_take_ant();
	ft_answer(now, now);
	g_all_rooms[now]->antn = 0;
	(*ants)--;
}
