/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ants.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/30 20:42:19 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/11 19:48:27 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

static void	ft_only_digits_in_line(char *line)
{
	unsigned i;

	i = 0;
	exit_if_empty(line);
	while (line[i] != '\n' && ft_isascii(line[i]))
	{
		if (ft_isdigit(line[i]))
			i++;
		else
			ft_error_msg();
	}
}

unsigned	any_ants(int fd)
{
	char	*line_content;

	while ((line_content = get_next_line_fd(fd)))
	{
		exit_if_empty(line_content);
		if (is_comment(line_content))
			free(line_content);
		else
			break ;
	}
	ft_only_digits_in_line(line_content);
	if (g_parse_status == 0)
		g_parse_status = 1;
	else
		ft_error_msg();
	g_ants = ft_atoi(line_content);
	free(line_content);
	if (g_ants <= 0)
		ft_error_msg();
	return (0);
}

unsigned	ft_take_ant(void)
{
	static unsigned i;

	if (++i <= (unsigned)g_ants)
		return (i);
	return (0);
}
