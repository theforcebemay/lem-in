/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/29 15:10:18 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/23 14:13:14 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

unsigned		g_parse_status = 0;
int				g_ants = 0;
t_room			*g_all_rooms[MAX_ROOM];
t_adj			*g_adj;

int				read_loop(int fd)
{
	char		*line_content;

	any_ants(fd);
	while ((line_content = get_next_line_fd(fd)))
	{
		exit_if_empty(line_content);
		if (strstr(line_content, "-") && !g_adj->adj)
			allocate_adj();
		parse_line(line_content, fd);
		free((void *)line_content);
	}
	if (!g_adj->adj)
		ft_error_msg();
	return (EXIT_SUCCESS);
}

void			ft_room_clean_up(void)
{
	unsigned	i;

	i = 0;
	while (g_all_rooms[i])
		free(g_all_rooms[i++]);
}

void			ft_check_fd_exit(int fd)
{
	if (fd == -1)
	{
		perror("Open error");
		exit(0);
	}
}

void			ft_clean_trace(t_llist *routes, t_llist *finals, int fd)
{
	ft_ldela(routes);
	ft_ldelastruct(finals);
	dealloimatrix(g_adj->adj, g_adj->size);
	ft_room_clean_up();
	free(g_adj);
	close(fd);
}

int				main(int argc, char **argv)
{
	int			fd;
	t_llist		*routes;
	t_llist		*finals;

	routes = NULL;
	g_adj = ft_calloc(1, sizeof(t_adj));
	fd = 0;
	if (argc == 2)
		fd = open(argv[argc - 1], O_RDONLY);
	ft_check_fd_exit(fd);
	read_loop(fd);
	routes = pre_solve(routes);
	ft_reverse_routes(routes);
	finals = ft_accumulate_paths(routes);
	if (!finals)
		ft_error_msg();
	ft_reverse_marching_tree(finals);
	ft_clean_trace(routes, finals, fd);
	return (0);
}
