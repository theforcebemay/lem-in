/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/27 19:54:43 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/12 18:42:05 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

int		dealloimatrix(int **mx, int row)
{
	while (--row >= 0)
		free(mx[row]);
	free(mx);
	return (EXIT_SUCCESS);
}

int		**alloadjmatrix(int size)
{
	int **adj;
	int i;

	i = 0;
	adj = ft_calloc((size_t)size, sizeof(int *));
	if (!adj)
		return (NULL);
	while (i < size)
		adj[i++] = ft_calloc((size_t)size, sizeof(int));
	return (adj);
}
