/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lists.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/30 21:56:41 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/12 18:44:05 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

int		search_name(t_room **start, char *name)
{
	unsigned i;

	i = 0;
	while (start[i])
	{
		if (!ft_strcmp(start[i]->name, name))
			return (i);
		i++;
	}
	return (-1);
}

int		search_by_status(unsigned int status, t_room **head)
{
	unsigned i;

	i = 0;
	while (head[i])
	{
		if (head[i]->status == status)
			return (i);
		i++;
	}
	return (-1);
}
