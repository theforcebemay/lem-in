/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   detecting_functions_2.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/23 13:45:32 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/23 13:46:25 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

int			is_command_end(const char *line_content)
{
	if (!ft_strcmp("##end\n", line_content))
		return (EXIT_FAILURE);
	return (EXIT_SUCCESS);
}

int			is_comment(const char *line_content)
{
	if (line_content[0] == '#' && line_content[1] != 0 &&
			(!is_command_end(line_content)) &&
					(!is_command_start(line_content)))
		return (EXIT_FAILURE);
	return (EXIT_SUCCESS);
}

char		*is_there_name(char *line)
{
	char	*start;

	start = line;
	while (*line == '|' || *line == '_' || *line == '-' || ft_isalnum(line[0]))
		line++;
	return ((line == start) ? NULL : line);
}

char		*is_there_whitespace_and_digits(char *line_content)
{
	char	*start;

	start = line_content;
	while (line_content[0] == ' ')
		line_content++;
	if (start == line_content)
		return (NULL);
	start = line_content;
	while (ft_isdigit(line_content[0]))
		line_content++;
	if (start == line_content)
		return (NULL);
	return (line_content);
}

int			is_room(const char *line_content)
{
	char	*f_result;

	if (!(f_result = is_there_name((char *)line_content)))
		return (EXIT_SUCCESS);
	if (!(f_result = is_there_whitespace_and_digits((char *)f_result)))
		return (EXIT_SUCCESS);
	if (!(f_result = is_there_whitespace_and_digits((char *)f_result)))
		return (EXIT_SUCCESS);
	if (f_result[0] == '\n')
		return (EXIT_FAILURE);
	return (EXIT_SUCCESS);
}
