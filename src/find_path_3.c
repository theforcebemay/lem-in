/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_path_3.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/23 13:53:13 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/23 13:54:57 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/lemin.h"

void			ft_adjust_route_structure(t_route **s, unsigned v)
{
	*s = ft_calloc(1, sizeof(t_route));
	(*s)->e = (unsigned)search_by_status(END_ROOM, g_all_rooms);
	(*s)->s = (unsigned)search_by_status(START_ROOM, g_all_rooms);
	ft_ldcpushb(&(*s)->route, &v, sizeof(unsigned));
	ft_ldcpushb(&(*s)->route, &(*s)->e, sizeof(unsigned));
}

void			bfs_inner(t_llist **queue, int *visited,
										unsigned *j, unsigned *i)
{
	while (*j < (unsigned)g_adj->size)
	{
		if (g_adj->adj[*i][*j] && !visited[*j] &&
					g_all_rooms[*i]->status != END_ROOM)
		{
			ft_ldcpushb(queue, j, sizeof(unsigned));
			if (!g_all_rooms[*j]->path && g_all_rooms[*j]->status != END_ROOM)
				g_all_rooms[*j]->path = g_all_rooms[*i]->path + 1;
		}
		visited[*i] = 1;
		(*j)++;
	}
}

int				bfs(unsigned int id, unsigned int aim, int *visited)
{
	unsigned	*i;
	unsigned	j;
	t_llist		*queue;

	queue = NULL;
	ft_ldcpushb(&queue, &id, sizeof(unsigned));
	while (queue)
	{
		i = (unsigned *)ft_ldpop(&queue);
		j = 0;
		if (visited[*i] || *i == aim)
		{
			free(i);
			continue;
		}
		bfs_inner(&queue, visited, &j, i);
		free(i);
	}
	free(visited);
	return (EXIT_SUCCESS);
}
