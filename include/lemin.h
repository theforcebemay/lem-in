/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem-in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/29 15:10:58 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/10/23 14:16:33 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_H
# define LEMIN_H
# include "lists.h"
# include "../libft/libft.h"
# include <unistd.h>
# include <fcntl.h>
# include <stdlib.h>
# include <ncurses.h>
# define MAX_NAME 256
# define MAX_ROOM 150000
# define REGULAR_ROOM 0
# define START_ROOM 1
# define END_ROOM 2

/*
**	type 0 : regular room, only one ant is allowed to be here
**	type 1 : start room, unlimited ants, path starts here
**	type 2 : end room, unlimited ants, path ends here
**	type 3 : regular room, but written in path variable
*/

typedef struct		s_adj
{
	int				**adj;
	int				size;
}					t_adj;

typedef struct		s_route
{
	t_llist			*route;
	unsigned		len;
	unsigned		used;
	unsigned		s;
	unsigned		e;
}					t_route;

typedef struct		s_room
{
	char			name[MAX_NAME];
	int				x;
	int				y;
	unsigned int	id;
	unsigned int	status : 2;
	unsigned int	antn;
	unsigned int	path;
	unsigned int	tracked : 1;
}					t_room;
void				ft_error_msg(void);
void				ft_march_room(unsigned now, unsigned next,
												unsigned e, unsigned *ants);
void				ft_march_list_cycle(t_route *path_struct, unsigned *ants);
void				ft_reverse_marching_tree(t_llist *routes);

void				is_valid_x_y(unsigned x, unsigned y, unsigned stop);
void				is_valid_name(char *name_searh, unsigned stop);

void				ft_do_nothing(void);

t_llist				*ft_accumulate_paths(t_llist *routes);
t_route				*ft_the_best_possible_path(t_route *path);
t_route				*ft_take_the_shortest_path(t_llist *routes);
void				ft_reverse_routes(t_llist *routes);
int					room_count(t_room **rooms);
int					room_record(const char *line_content, int fd);
unsigned			any_ants(int fd);
unsigned			ft_take_ant(void);
int					parse_line(const char *line_content, int fd);
int					exit_if_empty(const char *line_content);
int					is_command_links(const char *line_content);
int					is_command_start(const char *line_content);
int					is_command_end(const char *line_content);
int					is_comment(const char *line_content);
int					is_room(const char *line_content);
int					get_room_info(const char *line_content,
												unsigned int status);
int					get_link(const char *line_content, int **adj);
int					get_validate_rooms(t_room **room);
int					read_loop(int fd);
int					bfs(unsigned int id, unsigned int aim, int *visited);
void				print_matrix(int **m, int size);
unsigned			ft_llen(t_llist *head);
t_route				*ft_btrack_one_route(unsigned v);
t_llist				*ft_obtain_all_routes(unsigned v);
void				allocate_adj(void);
void				ft_ldelastruct(t_llist *head);

/*
**	Some useful functions i need to add to my libft
*/

int					ft_replace(char *where, char what, char to);

/*
**	Lists!
*/

int					search_name(t_room **start, char *name);

/*
**	Matrix functions
*/

int					dealloimatrix(int **mx, int row);
int					**alloadjmatrix(int size);
int					deallomatrix(char **mx, int row);
int					makevismatrix(char **matrix, int row, int col);
int					printmatrix(char **matrix);
char				**allomatrix(int row, int col);

/*
**	find_path.c
*/

t_route				*ft_btrack_one_route(unsigned v);
t_llist				*ft_obtain_all_routes(unsigned v);
t_llist				*pre_solve(t_llist *routes);
void				ft_march_room(unsigned now, unsigned next,
												unsigned e, unsigned *ants);
void				ft_march_list_cycle(t_route *path_struct, unsigned *ants);

/*
**	find_path_2.c
*/

int					goes_to_start(unsigned v, unsigned i);
int					is_start_room(unsigned v, t_route *route);
void				ft_delete_route(t_route **r);
t_llist				*ft_adj(unsigned v1);
t_route				*ft_take_the_shortest_path(t_llist *routes);

/*
**	find_path_3.c
*/

void				ft_adjust_route_structure(t_route **s, unsigned v);
int					bfs(unsigned int id, unsigned int aim, int *visited);
int					is_adj(unsigned v, unsigned i);

/*
**	find_path_4.c
*/

void				ft_march_cycle(t_llist *routes, unsigned *ants);
void				ft_reverse_marching_tree(t_llist *routes);
void				ft_answer(unsigned ant_room_id, unsigned room_name);
void				ft_blitzkrieg(unsigned now, unsigned *ants);

/*
**	struct_f.c
*/

int					search_by_status(unsigned int status, t_room **head);
int					search_name(t_room **start, char *name);

/*
**	Global vars
*/

extern t_room		*g_all_rooms[MAX_ROOM];
extern int			g_ants;
extern t_adj		*g_adj;
extern unsigned		g_parse_status;
#endif
