# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/06/29 15:27:58 by bsemchuk          #+#    #+#              #
#    Updated: 2017/10/23 14:20:30 by bsemchuk         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = lem-in

SRC = src/main.c\
	src/ants.c \
	src/detecting_functions.c\
	src/detecting_functions_2.c\
	src/find_path.c\
	src/find_path_2.c\
	src/find_path_3.c\
	src/find_path_4.c\
	src/line_parse.c\
	src/ft_destruct.c\
	src/useful.c\
	src/matrix.c\
	src/struct_f.c\
	src/ft_get_room_info.c\
	src/rooms_duplicate_check.c \
	src/ft_reverse_routes.c\
	src/lists.c\
	src/slists.c

OB = $(SRC:.c=.o)

FLAGS = -g -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OB)
	make -C libft/
	gcc $(FLAGS) -o $(NAME) $(OB) libft/libft.a -lncurses
%.o:%.c
	gcc $(FLAGS) -c -o $@ $<
clean:
	rm -f $(OB)
	make clean -C libft/
fclean: clean
	make fclean -C libft/
	rm -f $(NAME)
re: clean all
